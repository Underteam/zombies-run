﻿using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.AI;
#endif
using UnityEngine;

[ExecuteInEditMode]
public class GenerationLevel : MonoBehaviour
{
    [Header("Включить, если уровень с препятствиями")]
    public bool IsLet = false;
    [Header("Включить генератор уровней для редактора")]
    public bool IsUseGenerationLevel = false;

    [Header("Finish zone")] public FinishZone finish;
    [Header("Препятствие")] public GameObject fence;
    [Space]
    public List<ObjectRepeater> repeatersPrefabsList = new List<ObjectRepeater>();

    public static GenerationLevel Instance;

    private void Awake()
    {
        Instance = this;
    }

    [Header("")][Space]
    [Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space][Space]
    [Space][Space][Space][Space][Space][Space][Space][Space][Space]
    [Header("")]
    public List<ObjectRepeater> repeaters = new List<ObjectRepeater>();

    [ContextMenu("Запечь NavMesh")]
    public void NavigationBake()
    {
#if UNITY_EDITOR
         NavMeshBuilder.BuildNavMesh();
#endif
    }

    [ContextMenu("Поставить препятствие в центр")]
    public void CreateFenceOnCenter()
    {
        var obj = Instantiate(fence);
        fence = obj;
        float center = Vector3.Distance(repeaters[0].transform.position, repeaters[repeaters.Count - 2].transform.position);
        fence.transform.position = new Vector3(center / 2, fence.transform.position.y, fence.transform.position.z);
    }

    [ContextMenu("GenerateCity")]
    private void GenerateCity()
    {
        var temp = new List<ObjectRepeater>();
        for (int i = 0; i < repeatersPrefabsList.Count; i++)
        {
            if (repeatersPrefabsList[i] != null)
                temp.Add(repeatersPrefabsList[i]);
        }

        repeatersPrefabsList = temp;

        if (repeatersPrefabsList.Count <= 0) return;
        //var repeater =  Instantiate(repeatersPrefabsList[0]);
        var repeater = repeatersPrefabsList[0];
        repeater.transform.position = Vector3.zero;
        repeater.AddCollider();
        repeaters.Add(repeater);
        
        for (int i = 1; i < repeatersPrefabsList.Count; i++)
        {
            //var obj = Instantiate(repeatersPrefabsList[i]);
            var obj = repeatersPrefabsList[i];
            obj.prevObj = repeaters[i - 1];
            obj.transform.position = repeaters[i - 1].transform.position + Vector3.right * (repeaters[i - 1].distanceToRepeat / 2 + obj.distanceToRepeat / 2 + repeaters[i - 1].offset.x);
            obj.AddCollider();
            repeaters.Add(obj);
        }

        repeatersPrefabsList = repeaters;
    }

    private void ChangeCityPositions()
    {
        repeaters[0].transform.position = Vector3.zero;
        for (int i = 1; i < repeaters.Count; i++)
        {
            repeaters[i].transform.position = repeaters[i - 1].transform.position + Vector3.right * (repeaters[i - 1].distanceToRepeat / 2 + repeaters[i].distanceToRepeat / 2 + repeaters[i - 1].offset.x);
        }
        repeatersPrefabsList = repeaters;
    }

    private void CreateCity()
    {
        List<ObjectRepeater> tempRepeaters = new List<ObjectRepeater>();

        int index = repeaters.FindIndex(info => info == repeatersPrefabsList[0]);

        if (index != 0)
        {
            DestroyImmediate(repeaters[0].gameObject);
            //var obj = Instantiate(repeatersPrefabsList[0]);
            var obj = repeatersPrefabsList[0];
            tempRepeaters.Add(obj);
            obj.AddCollider();
        }
        else
        {
            tempRepeaters.Add(repeaters[0]);
            repeaters[0].transform.position = Vector3.zero;
        }

        for (int i = 1; i < repeatersPrefabsList.Count; i++)
        {
            if (i <= repeaters.Count - 1)
            {
                if (repeatersPrefabsList[i] != repeaters[i])
                {
                    DestroyImmediate(repeaters[i].gameObject);
                    //var obj = Instantiate(repeatersPrefabsList[i]);
                    var obj = repeatersPrefabsList[i];
                    obj.AddCollider();
                    obj.transform.position = tempRepeaters[i - 1].transform.position + Vector3.right * (tempRepeaters[i - 1].distanceToRepeat / 2 + obj.distanceToRepeat / 2 + tempRepeaters[i - 1].offset.x);
                    tempRepeaters.Add(obj);
                }
                else
                {
                    repeaters[i].transform.position = tempRepeaters[i - 1].transform.position + Vector3.right * (tempRepeaters[i - 1].distanceToRepeat / 2 + repeaters[i].distanceToRepeat / 2 + tempRepeaters[i - 1].offset.x);
                    tempRepeaters.Add(repeaters[i]);
                }
            }
            else
            {
                //                var obj = Instantiate(repeatersPrefabsList[i]);
                var obj = repeatersPrefabsList[i];
                obj.AddCollider();
                obj.transform.position = tempRepeaters[i - 1].transform.position + Vector3.right * (tempRepeaters[i - 1].distanceToRepeat / 2 + obj.distanceToRepeat / 2 + tempRepeaters[i - 1].offset.x);
                tempRepeaters.Add(obj);
            }
        }

        repeaters = tempRepeaters;

        repeatersPrefabsList.Clear();
        for (int i = 0; i < repeaters.Count; i++)
        {
            repeatersPrefabsList.Add(repeaters[i]);
        }
    }


    private void DestroyCity()
    {
        for (int i = 0; i < repeaters.Count; i++)
        {
            if (!repeatersPrefabsList.Contains(repeaters[i]))
                DestroyImmediate(repeaters[i].gameObject);
        }
    }


    private void CheckChangeCity()
    {
        Canvas.ForceUpdateCanvases();
        if (repeatersPrefabsList.Count <= 0)
        {
            repeaters.Clear();
            repeatersPrefabsList.Clear();
            return;
        }
        
        if (repeaters.Count <= 0)
        {
            GenerateCity();
        }
        else
        {
            var tempRepeaters = new List<ObjectRepeater>();
            bool isDestroy = false;

            for (int i = 0; i < repeatersPrefabsList.Count; i++)
            {
                if (repeatersPrefabsList[i] == null)
                    isDestroy = true;
            }

            if (isDestroy && repeatersPrefabsList.Count == repeaters.Count)
            {
                DestroyCity();
            }

            for (int i = 0; i < repeaters.Count; i++)
            {
                if (repeaters[i] != null)
                    tempRepeaters.Add(repeaters[i]);
                else
                    isDestroy = true;
            }

            repeaters = tempRepeaters;

            if (isDestroy)
            {
                ChangeCityPositions();
            }

            if (repeaters.Count < repeatersPrefabsList.Count || repeaters.Count == repeatersPrefabsList.Count)
            {
                CreateCity();
            }
           
        }
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (IsUseGenerationLevel)
            CheckChangeCity();
    }
#endif
}
