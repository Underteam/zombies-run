﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FailPanel : MonoBehaviour
{
    [Header("Панель возраждения")]
    public Button reviveButton;
    public Button noThanksButton;
    public Image reviveProgressBar;
    public float reviveTimer = 5f;
    [Space]
    public Button tapToContinueButton;
    public Text failText;
    public Color deathFormZombieColor;
    public string deathFromZombie = "You became a zombie!";
    public Color deathFormCarColor;
    public string deathFromCar = "Car accident!";
    public Color savedNotAllColor;
    public string savedNotAll = "You didn't rescue everyone";
    public int reviveCount = 1;

    private void Start()
    {
        if(reviveButton != null)
            reviveButton.onClick.AddListener(Revive);
        if(noThanksButton != null)
            noThanksButton.onClick.AddListener(Restart);
        if(tapToContinueButton != null)
            tapToContinueButton.onClick.AddListener(Restart);
    }

    private void OnEnable()
    {
        if(failText == null) return;
        SetFail(GameController.Instance.FailType);
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Revive()
    {
        RemoveZombies();
        ObserverPattern.Instance.OnClearPoolCarsAction?.Invoke();
        ObserverPattern.Instance.OnRevivePlayerAction?.Invoke();
        ObserverPattern.Instance.OnShowUIAction?.Invoke();
        ObserverPattern.Instance.OnRestartAgentAction?.Invoke();
        GameController.Instance.IsDontCanControl = false;
        ObserverPattern.Instance.OnChangeNumbersZobieIntoPeopleAction?.Invoke(1);

        gameObject.SetActive(false);
    }

    private void RemoveZombies()
    {
        var zombies = GameController.Instance.GetZomvies;
        for (int i = 0; i < zombies.Count; i++)
        {
            if (!zombies[i].IsPeople)
            {
               zombies[i].gameObject.SetActive(false);
            }
        }
    }

    private void SetFail(FailTypeEnum type)
    {
        failText.text = deathFromZombie;
        failText.color = deathFormZombieColor;
        tapToContinueButton.gameObject.SetActive(false);
        noThanksButton.gameObject.SetActive(false);
        reviveButton.gameObject.SetActive(false);
        switch (type)
        {
            case FailTypeEnum.DeathFromZombie:
                failText.text = deathFromZombie;
                failText.color = deathFormZombieColor;
                ReviveShow();
                break;
            case FailTypeEnum.DeathFromCar:
                failText.text = deathFromCar;
                failText.color = deathFormCarColor;
                ReviveShow();
                break;
            case FailTypeEnum.SavedNotAll:
                failText.text = savedNotAll;
                failText.color = savedNotAllColor;
                tapToContinueButton.gameObject.SetActive(true);
                break;
        }
    }

    private void ReviveShow()
    {
        if (reviveCount <= 0)
            tapToContinueButton.gameObject.SetActive(true);
        else
            StartReviveTimer();
    }

    private void StartReviveTimer()
    {
        if (reviveButton != null)
        {
            reviveCount -= 1;
            reviveButton.gameObject.SetActive(true);
            StartCoroutine(reviveCoroutine());
        }
    }

    private IEnumerator reviveCoroutine()
    {
        float time = 0f;
        bool showNoThanksButton = false;
        while (time < reviveTimer)
        {
            time += Time.deltaTime;
            if (time >= 1f && !showNoThanksButton)
            {
                showNoThanksButton = true;
                noThanksButton.gameObject.SetActive(true);
            }
            reviveProgressBar.fillAmount = (reviveTimer - time) / reviveTimer;
            yield return null;
        }
       Restart();
    }
}

public enum FailTypeEnum
{
    DeathFromZombie,
    DeathFromCar,
    SavedNotAll
}
