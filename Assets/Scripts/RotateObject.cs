﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public float speed = 350f;
    public bool IsRotateX;
    public bool IsRotateY;
    public bool IsRotateZ;

    private void Update()
    {
        if(IsRotateX)
            transform.Rotate(Vector3.right * speed * Time.deltaTime);

        if (IsRotateY)
            transform.Rotate(Vector3.up * speed * Time.deltaTime);

        if (IsRotateZ)
            transform.Rotate(Vector3.forward * speed * Time.deltaTime);
    }
}
