﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform target;
    public Vector3 offset;
    public Quaternion rotation;

    public bool updateY = true;
    public bool updateZ = false;

    public bool lerpOn = false;
    public float lerpSpeed = 10;

    public GameObject confettiEffect;
    public static CameraFollow instance = null;

    private void Awake () {
        instance = this;
    }

    [ContextMenu("Save")]
    public void Save () {
        if (target == null)
            return;

        offset = transform.position - target.position;
        rotation = transform.rotation;
    }

    public void Adapt () {
        if (target == null)
            return;

        Vector3 targetPos = (target.position + offset);
        targetPos = new Vector3 (targetPos.x, (updateY) ? targetPos.y : transform.position.y, updateZ ? targetPos.z : transform.position.z);

        if (lerpOn)
            transform.position = Vector3.Lerp (transform.position, targetPos, Time.deltaTime * lerpSpeed);
        else
            transform.position = targetPos;

        transform.rotation = rotation;
    }

    private void LateUpdate () {
        Adapt ();
    }
}