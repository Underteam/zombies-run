﻿using RootMotion.Dynamics;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public GameObject BackCollider;
    public PuppetMaster puppetMaster;

    public BehaviourFall behFall;
    public Rigidbody playerRigidbody;
    private VariableJoystick joystick;
    public Animator playerAnimator;
    public float speedMove = 3f;
    public float speedRotate = 1.5f;
    public float speedArrowRotate = 40f;
    public float speedJoystickRotateY = 100f;

    public float impulseForce = 25f;

    private bool isMove = false;
    public bool isDeath = false;

    public bool IsCanTurn { get; set; }
    public bool IsJumping { get; set; }

    //[HideInInspector]
    public Collider coll = null;

    public Transform itemPosition;
    public GameObject carBangEffect;
    public ParticleSystem smokeEffect;
    public Transform peopleSavingTextPosition;

    public void Awake()
    {
        if (coll == null)
            coll = GetComponent<Collider>();
    }

    private float maxDistanceFromPlayerToFinish;
    private float currentDistanceFromPlayerToFinish;
    private Transform finish;

    private Item currentItem;
    public bool isCarryItem = false;

    private void Start()
    {
        if (ObserverPattern.Instance != null)
        {
            ObserverPattern.Instance.OnPlayerDeathAction += Death;
            ObserverPattern.Instance.OnPlayerCanTurnAction += SetCanTurn;
            ObserverPattern.Instance.OnPlayerFailAction += Fail;
            ObserverPattern.Instance.OnPlayerStateIdleAction += Idle;
            ObserverPattern.Instance.OnDeathForceAction += DeathForce;
            ObserverPattern.Instance.OnRemoveItemAction += RemoveItem;
            ObserverPattern.Instance.OnRevivePlayerAction += Revive;
        }

        if (GameController.Instance != null)
        {
            joystick = GameController.Instance.joystick;
            finish = GenerationLevel.Instance.finish.hawkGameObject.transform;
            GameController.Instance.Player = this;
        }

        if (finish != null)
        {
            maxDistanceFromPlayerToFinish = Vector3.Distance(transform.position, finish.position);
            currentDistanceFromPlayerToFinish = maxDistanceFromPlayerToFinish;
        }

        if (CameraFollow.instance != null)
            CameraFollow.instance.target = gameObject.transform;

        if (BackCollider != null)
        {
            var backCol = Instantiate(BackCollider);
            backCol.transform.position = new Vector3(transform.position.x - 73f, transform.position.y, transform.position.z);
        }
    }

    private void SetCanTurn(bool isCan)
    {
        IsCanTurn = isCan;
    }

    private Vector3 mouseStartPosition;

    void Update()
    {
        if (!GameController.Instance.IsGameStarted) return;

        if (finish != null)
        {
            currentDistanceFromPlayerToFinish = Vector3.Distance(transform.position, finish.position);
            if (ObserverPattern.Instance != null)
                ObserverPattern.Instance.OnChangeProgressLevelAction?.Invoke(currentDistanceFromPlayerToFinish, maxDistanceFromPlayerToFinish);
        }

        if (IsJumping) return;
        if (GameController.Instance != null && GameController.Instance.IsDontCanControl) return;


        if (!isDeath)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                mouseStartPosition = Input.mousePosition;
            }
            if (GameController.Instance != null && GameController.Instance.IsAutoRun)
            {
                if (isMove)
                {
                    transform.Translate(Vector3.forward * speedMove * Time.deltaTime);
                    if (smokeEffect != null)
                        smokeEffect.Emit(1);
                }
                else
                {
                    isMove = true;
                    playerAnimator.Play(isCarryItem ? "Carry" : "Run");
                    playerRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
                    if (smokeEffect != null && !smokeEffect.gameObject.activeSelf)
                        smokeEffect.gameObject.SetActive(true);
                }

                if (Input.GetKey(KeyCode.Mouse0))
                    PlayerRotate();
            }
            else if (Input.GetKey(KeyCode.Mouse0))
            {
                PlayerMoveForward();
            }
            else
            {
                Idle();
            }
        }
    }

    public void SetItem(Item item)
    {
        if (currentItem != null)
            Destroy(currentItem.gameObject);

        currentItem = item;

        if (currentItem.partricles != null)
            currentItem.partricles.gameObject.SetActive(false);
        currentItem.transform.SetParent(itemPosition);
        currentItem.transform.localPosition = Vector3.zero;
        currentItem.transform.localRotation = Quaternion.identity;
        currentItem.transform.localScale /= 2f;
        isCarryItem = true;
    }

    private void RemoveItem()
    {
        isCarryItem = false;
        playerAnimator.Play(isMove ? "Run" : "Idle");
        DestroyItem();
    }

    public void DestroyItem()
    {
        if(currentItem != null)
            Destroy(currentItem.gameObject);
    }

    public void DisableItem()
    {
        if (currentItem != null)
        {
            currentItem.gameObject.SetActive(false);
            ObserverPattern.Instance.OnItemHideAction?.Invoke();
        }
    }
    

    public void JumpingActive(bool isActive)
    {
        IsJumping = isActive;
    }

    private void PlayerMoveForward()
    {
        if (isMove)
        {
            if (GameController.Instance.ControlType == ControlTypeEnum.JoystickControlAll)
            {
                if (Vector3.Distance(mouseStartPosition, Input.mousePosition) > 5f)
                {
                    Vector3 mouseDelta = Input.mousePosition - mouseStartPosition;
                    Vector3 normolized = mouseDelta.normalized;

                    Vector3 rotate = new Vector3(normolized.y, 0, -normolized.x);
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rotate, Vector3.up), speedRotate * Time.deltaTime);
                }

                transform.Translate(Vector3.forward * speedMove * Time.deltaTime);
                if (smokeEffect != null) smokeEffect.Emit(1);
            }
        }
        else
        {
            isMove = true;
            playerAnimator.Play(isCarryItem ? "Carry" : "Run");
            playerRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
            if (smokeEffect != null && !smokeEffect.gameObject.activeSelf)
                smokeEffect.gameObject.SetActive(true);
        }
    }

    private void PlayerRotate()
    {
        if (GameController.Instance.ControlType == ControlTypeEnum.ButtonsControl)
        {
            if (GameController.Instance.IsMoveLeft)
                transform.Rotate(Vector3.up * -speedArrowRotate * Time.deltaTime);

            if (GameController.Instance.IsMoveRight)
                transform.Rotate(Vector3.up * speedArrowRotate * Time.deltaTime);
        }
    }

    private void Idle()
    {
        if (isMove)
        {
            playerAnimator.Play(isCarryItem ? "CarryIdle" : "Idle");
            isMove = false;
            playerRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            //if (smokeEffect != null)
            //    smokeEffect.SetActive(false);
        }
    }

    private void Fail()
    {
        playerRigidbody.isKinematic = true;
        playerAnimator.enabled = false;
        isMove = false;
        ObserverPattern.Instance.OnCloseUIElementsAction?.Invoke();
        isDeath = true;
    }

    private void Revive()
    {
        behFall.enabled = false;
        //puppetMaster.state = PuppetMaster.State.Alive;
        puppetMaster.mode = PuppetMaster.Mode.Kinematic;
        isDeath = false;
        if (playerAnimator != null)
        {
            playerAnimator.enabled = true;
            playerAnimator.Play("Idle");
        }

        if (currentItem != null)
        {
            currentItem.gameObject.SetActive(true);
            ObserverPattern.Instance.OnItemShowAction?.Invoke();
        }
    }
    
    private void DeathForce(Vector3 force)
    {
        behFall.enabled = true;
        Time.timeScale = 0.5f;
        this.force = force * impulseForce;
        Canvas.ForceUpdateCanvases();
        Invoke("AddForce", 0.15f);
    }

    private void AddForce()
    {
        var muscles = puppetMaster.muscles;
        if (force != null)
        {
            for (int i = 0; i < muscles.Length; i++)
            {
                var rigid = muscles[i].joint.GetComponent<Rigidbody>();
                rigid.AddForce(force.Value, ForceMode.VelocityChange);
            }
        }

        Invoke("OpenFailPanelInvoke", 0.5f);
    }

    private void OpenFailPanelInvoke()
    {
        ObserverPattern.Instance.OnOpenFailPanelAction?.Invoke();
        ObserverPattern.Instance.OnCloseUIElementsAction?.Invoke();
        Time.timeScale = 1f;
    }

    private Vector3? force;

    private void Death()
    {
        playerAnimator.Play("Death");
        isMove = false;
        isDeath = true;
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        ObserverPattern.Instance.OnOpenFailPanelAction?.Invoke();
        ObserverPattern.Instance.OnCloseUIElementsAction?.Invoke();
        ObserverPattern.Instance.OnItemHideAction?.Invoke();
    }
}
