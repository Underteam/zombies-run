﻿using UnityEngine;

public class Car : MonoBehaviour
{
    public Collider carCollider;
    public float impulse = 25f;
    public float angle = 45f;
    private PlayerController player;
    private float currentSpeed = 0.2f;

    public bool IsMove = true;

    private ZombieController zombie;

    public void Init(float speed)
    {
        currentSpeed = speed;
        IsMove = true;
    }


    private void OnCollisionEnter(Collision other)
    {
        player = other.gameObject.GetComponent<PlayerController>();
        zombie = other.gameObject.GetComponent<ZombieController>();

        if (zombie != null)
        {
            zombie.Death();
        }

        if (player == null) return;
        if (!IsCollisionPointForward(other.contacts[0].point)) return;
        player.carBangEffect.transform.SetParent(player.transform.parent.parent);
        player.carBangEffect.transform.position = other.contacts[0].point + Vector3.up;
        player.carBangEffect.SetActive(true);
        player.DisableItem();
        GameController.Instance.FailType = FailTypeEnum.DeathFromCar;
        KillPlayer();
    }

    private void KillPlayer()
    {
        ObserverPattern.Instance.OnPlayerFailAction?.Invoke();
        ObserverPattern.Instance.OnStopAllCarAction?.Invoke();
        ObserverPattern.Instance.OnZombieStateIdleAction?.Invoke();
        Vector3 addForce = (player.transform.position - (transform.position - Vector3.up));
        ObserverPattern.Instance.OnDeathForceAction?.Invoke(addForce);
    }

    private bool IsCollisionPointForward(Vector3 point)
    {
        Vector3 rightCorner = new Vector3(carCollider.bounds.size.x / 2f, 0f,carCollider.bounds.size.z / 2f) + carCollider.bounds.center;
        Vector3 forward = new Vector3(transform.forward.x, 0f, transform.forward.z);
        float forwardAngle = Vector3.Angle(rightCorner - carCollider.bounds.center, forward);
        float targetAngle = Vector3.Angle(forward, point - carCollider.bounds.center);
        return targetAngle < forwardAngle;
    }

    private void FixedUpdate()
    {
        if(IsMove)
            Move();
    }

    private void Move()
    {
        gameObject.transform.Translate(Vector3.forward * currentSpeed);
    }

}
