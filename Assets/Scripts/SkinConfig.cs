﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "SkinConfig", menuName = "Data/SkinConfig")]
public class SkinConfig : ScriptableObject
{
    public Mesh skinMesh;
    public Material skinMaterial;
    public float speedMove;
}
