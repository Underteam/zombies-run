﻿using UnityEngine;

[CreateAssetMenu(fileName = "ItemConfig", menuName = "Data/ItemConfig")]
public class ItemConfig : ScriptableObject
{
    public Mesh itemMesh;
}
