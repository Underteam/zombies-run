﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeFontSize : MonoBehaviour
{
    public int fontSize = 68;
    public bool isChangeSize = false;
    private Text currentText;
    private void Start()
    {
        isChangeSize = false;
        currentText = GetComponent<Text>();
        fontSize = currentText.fontSize;
    }

    private void Update()
    {
        if (isChangeSize)
        {
            currentText.fontSize = fontSize;
        }
    }
}
