﻿using UnityEngine;

public class ObjectMoveUp : MonoBehaviour
{
    public Animator anim;

    private void Start()
    {
        ObserverPattern.Instance.OnHawkMoveUpAction += MoveUp;
    }


    private void MoveUp()
    {
        if(anim != null)
            anim.Play("HawkMove");
    }
}
