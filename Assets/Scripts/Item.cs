﻿using UnityEngine;

[ExecuteInEditMode]
public class Item : MonoBehaviour
{
    [SerializeField] private MeshFilter itemMesh;
    [SerializeField] private MeshCollider collider;
    [SerializeField] private ItemConfig item;
    [SerializeField] private RotateObject rotate;
    public GameObject partricles;

    private PlayerController player;

    private void Awake()
    {
        if (rotate == null)
            rotate = GetComponent<RotateObject>();
    }

    private void OnTriggerEnter(Collider other)
    {
        player = other.GetComponent<PlayerController>();
        if (player != null && !player.isDeath)
        {
            player.SetItem(this);
            if (rotate != null) rotate.enabled = false;
            //ObserverPattern.Instance.OnPlayerCanTurnAction?.Invoke(true);
            ObserverPattern.Instance.OnItemActivateAction?.Invoke();
            player.playerAnimator.Play("Carry");
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        SetItemSkin();
    }
#endif

    private void SetItemSkin()
    {
        if (itemMesh != null && item != null && collider != null)
        {
            itemMesh.mesh = item.itemMesh;
            collider.sharedMesh = item.itemMesh;
        }
    }
}
