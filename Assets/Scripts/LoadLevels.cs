﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevels : MonoBehaviour
{
    private void Awake()
    {
        int levelIndex = PlayerPrefs.GetInt("Level");
        if (levelIndex == 0) levelIndex = 1;
        SceneManager.LoadScene(levelIndex);
    }
}
