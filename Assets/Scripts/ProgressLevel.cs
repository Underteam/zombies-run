﻿using UnityEngine;
using UnityEngine.UI;

public class ProgressLevel : MonoBehaviour
{
    public Text currentLevelText;
    public Text nextLevelText;
    public Image progressLevel;
    [Header("Фон прогресс бара без препятствий")]
    public Sprite progressBackWithoutObstacles;
    [Header("Фон прогресс бара с препятствиями")]
    public Sprite progressBackWithObstacles;
    [Header("Прогресс бар без препятствий")]
    public Sprite progressBarWithoutObstacles;
    [Header("Прогресс бар с препятствиями")]
    public Sprite progressBarWithObstacles;

    private void Start()
    {
        ObserverPattern.Instance.OnChangeProgressLevelAction += ChangeProgressLevel;
        ChangeSprites();
        if (currentLevelText != null)
            currentLevelText.text = PlayerData.Instance.LevelCompletedCount.ToString();

        if (nextLevelText != null)
            nextLevelText.text = (PlayerData.Instance.LevelCompletedCount + 1).ToString();
        if(progressLevel != null)
            progressLevel.fillAmount = 0;
    }

    private void ChangeProgressLevel(float currentDistance, float maxDistance)
    {
        if (progressLevel != null)
            progressLevel.fillAmount = (maxDistance - currentDistance) / maxDistance;
    }

    private void ChangeSprites()
    {
        GetComponent<Image>().sprite = GenerationLevel.Instance.IsLet ? progressBackWithObstacles : progressBackWithoutObstacles;
        progressLevel.sprite = GenerationLevel.Instance.IsLet ? progressBarWithObstacles : progressBarWithoutObstacles;
    }
}
