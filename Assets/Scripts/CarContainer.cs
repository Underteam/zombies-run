﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "CarContainer", menuName = "Data/CarContainer")]
public class CarContainer : ScriptableObject
{
    public List<Car> carList = new List<Car>();
}
