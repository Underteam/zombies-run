﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkinsPeopleConfig", menuName = "Data/SkinsPeopleConfig")]
public class SkinsPeopleConfig : ScriptableObject
{
    public SkinConfig defaultPeopleSkin;
    public List<SkinConfig> peopleSkinList = new List<SkinConfig>();

    public SkinConfig GetRandomSkin()
    {
        if (peopleSkinList.Count <= 0)
        {
            Debug.LogError("Skins people config = null");
            return defaultPeopleSkin != null ? defaultPeopleSkin : null;
        }
        int randomIndex = Random.Range(0, peopleSkinList.Count);
        return peopleSkinList[randomIndex];
    }
}

