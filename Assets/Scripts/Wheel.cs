﻿using UnityEngine;

public class Wheel : MonoBehaviour
{
    public float speed = 350f;

    private void Update()
    {
        transform.Rotate(Vector3.right * speed *  Time.deltaTime);
    }

}
