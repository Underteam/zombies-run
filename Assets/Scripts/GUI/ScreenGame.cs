﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenGame : MonoBehaviour
{
    [Header("Панель смерти")]
    public GameObject failPanel;
    [Header("Панель выигрыша")]
    public GameObject winPanel;

    [Header("Панель с количеством спасенных людей")]
    public GameObject peopleSavingProgress;

    [Header("Панель с прогрессом пройденного пути")]
    public GameObject progressLevelPanel;

    public GameObject tapToStart;

    public GameObject joyStickAllTest;
    public GameObject buttonsControlTest;

    private void Start()
    {
        //ObserverPattern.Instance.OnStartLevelAction += StartLevel;
        ObserverPattern.Instance.OnShowUIAction += ShowUI;
        ObserverPattern.Instance.OnCloseUIElementsAction += CloseUI;
        ObserverPattern.Instance.OnOpenTapToStartAction += OpenTapToStart;
        ObserverPattern.Instance.OnOpenFailPanelAction += FailPanel;
        ObserverPattern.Instance.OnPlayerWinAction += () =>
        {
            
            winPanel.gameObject.SetActive(true);
            GameController.Instance.IsDontCanControl = true;
            ObserverPattern.Instance.OnPlayerStateIdleAction?.Invoke();
        };

        if (tapToStart != null)
            tapToStart.SetActive(true);

        if (joyStickAllTest != null)
            joyStickAllTest.SetActive(true);

        if (buttonsControlTest != null)
            buttonsControlTest.SetActive(true);

        Time.timeScale = 1f;
    }

    private void CloseUI()
    {
        peopleSavingProgress.SetActive(false);
        progressLevelPanel.SetActive(false);
        if (joyStickAllTest != null)
            joyStickAllTest.SetActive(false);
        if (buttonsControlTest != null)
            buttonsControlTest.SetActive(false);
    }

    private void ShowUI()
    {
        peopleSavingProgress.SetActive(true);
        progressLevelPanel.SetActive(true);
    }

    private void OpenTapToStart()
    {
        if (tapToStart != null)
            tapToStart.SetActive(true);
    }

    public void StartLevel()
    {
        ObserverPattern.Instance.OnStartLevelAction?.Invoke();
        if(tapToStart != null)
            tapToStart.SetActive(false);
        peopleSavingProgress.SetActive(GameController.Instance.IsNeedSavingPeople);
        progressLevelPanel.SetActive(true);
        GameController.Instance.IsGameStarted = true;
        Canvas.ForceUpdateCanvases();
    }

    private void FailPanel()
    {
        if (failPanel != null)
            failPanel.SetActive(true);
        GameController.Instance.IsDontCanControl = true;
    }
}
