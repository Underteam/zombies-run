﻿using UnityEngine;
using UnityEngine.UI;

public class ScreenWin : MonoBehaviour
{
    public Button restartButton;

    private void Start()
    {
        if(restartButton != null)
            restartButton.onClick.AddListener(NextLevel);
    }

    private void NextLevel()
    {
        PlayerData.Instance.NextLevel();
    }
}
