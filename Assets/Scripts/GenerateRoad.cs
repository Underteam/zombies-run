﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class GenerateRoad : MonoBehaviour
{
    [Header("Левый кусок дороги")]
    public GameObject leftRoad;
    [Header("Правый кусок дороги")]
    public GameObject rightRoad;
    [Header("Количество создаваемых элементов")]
    public int createCount;

    public float stepPositionZ = -5f;
    [ContextMenu("CreateRoads")]
    private void CreateRoads()
    {
        for (int i = 0; i < createCount; i++)
        {
            var left = Instantiate(leftRoad, leftRoad.transform.parent);
            left.transform.position = leftRoad.transform.position + (Vector3.left * stepPositionZ);
            left.gameObject.name = "Road";
            leftRoad = left;

            var right = Instantiate(rightRoad, rightRoad.transform.parent);
            right.transform.position = rightRoad.transform.position + (Vector3.left * stepPositionZ);
            right.gameObject.name = "Road";
            rightRoad = right;
        }
    }






}
