﻿using UnityEngine;
using UnityEngine.AI;

public class ZombieController : MonoBehaviour
{
    public Rigidbody zombieRigidbody;
    public Animator zombieAnimator;
    public float speeMove;
    public float speedRotate;
    public PlayerController target;
    public NavMeshAgent agent;
    public SkinedController skinController;
    public BoxCollider boxCollider;

    public bool IsPeople = false;
    public bool IsJumping { get; set; }

    private void Start()
    {
        agent.speed = speeMove;
        IsPeople = false;
        ObserverPattern.Instance.OnZombieStateIdleAction += StopRun;
        ObserverPattern.Instance.OnRestartAgentAction += RestartAgent;
        Idle();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(IsPeople) return;
        
        if (other.GetComponent<PlayerController>() != null)
        {
            startPosition = transform.position;
            target = other.GetComponent<PlayerController>();
            resetAgent = false;
            RunToTarget();
        }
    }

    public void Death()
    {
        if (zombieAnimator != null)
        {
            agent.isStopped = true;
            target = null;
            zombieAnimator.SetTrigger("IsDeath");
            zombieAnimator.Play("Death");
        }
    }

    public void JumpingActive(bool isActive)
    {
        IsJumping = isActive;
    }


    private void RunToTarget()
    {
        if(zombieAnimator != null)
            zombieAnimator.Play("Run");
        if(!GameController.Instance.GetZomvies.Contains(this))
            GameController.Instance.GetZomvies.Add(this);
    }

    public void ChangeSpeed(float speed)
    {
        agent.speed = speed;
    }

    private float timer = 0f;
    private Vector3 startPosition = Vector3.zero;
    private bool resetAgent = false;


    private void Update()
    {
        if (IsJumping || IsPeople) return;

        if (resetAgent)
        {
            timer += Time.deltaTime;
            if (timer >= 0.5f)
            {
                agent.enabled = true;
                resetAgent = false;
            }
        }

        if (target != null)
        {
            if (agent.isActiveAndEnabled)
                agent.SetDestination(target.transform.position);

            //timer += Time.deltaTime;
            //if (timer >= 0.1f && !resetAgent)
            //{
            //    float distance = Vector3.Distance(startPosition, transform.position);
            //    if (distance < 0.2f)
            //    {
            //        if (agent.enabled)
            //            agent.enabled = false;
            //        else
            //        {
            //            if (timer >= 0.15f)
            //            {
            //                agent.enabled = true;
            //                resetAgent = true;
            //            }
            //        }
            //    }
            //}

            //if (IsPeople)
            //{
            //    if (Vector3.Distance(transform.position, target.gameObject.transform.position) <= agent.stoppingDistance)
            //    {
            //        if (zombieAnimator != null)
            //            zombieAnimator.Play("Idle");
            //        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            //    }
            //    else
            //    {
            //        PeopleRun();
            //    }
            //}
        }
    }

    private void RestartAgent()
    {
        agent.enabled = false;
        timer = 0;
        resetAgent = true;
        //agent.enabled = false;
        //agent.enabled = true;
        //if (agent.isActiveAndEnabled)
        //    agent.isStopped = true;
    }




    private void Idle()
    {
        if(zombieAnimator != null)
            zombieAnimator.Play("Idle");
        target = null;
    }

    public void TurnIntoPeople()
    {
        if (boxCollider != null) boxCollider.enabled = false;
        IsPeople = true;
        agent.isStopped = true;
        target = null;
        if (skinController != null) skinController.SetPeopleSkin();
        if (zombieAnimator != null)
        {
            zombieAnimator.SetTrigger("IsVictory");
            zombieAnimator.Play("Victory");
        }

        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        
        ObserverPattern.Instance.OnChangeNumbersZobieIntoPeopleAction?.Invoke(1);
        ObserverPattern.Instance.OnAddPeopleAction?.Invoke(this);
    }

    private void PeopleRun()
    {
        if(zombieAnimator != null)
            zombieAnimator.Play("PeopleRun");
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        agent.stoppingDistance = 2f;
        IsPeople = true;
    }

    private void StopRun()
    {
        target = null;
        if(zombieAnimator != null)
            zombieAnimator.Play("Idle");
        if (agent != null && agent.enabled)
            agent.isStopped = true;
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }
}
