﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerData : MonoBehaviour
{
    public static PlayerData Instance = null;
    public bool IsClearData = false;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            if(IsClearData)
                LoadDefault();
            else
                Load();
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
    }

    public int Level;
    public int LevelMax = 30;
    public int LevelCompletedCount = 1;

    private void LoadDefault()
    {
        PlayerPrefs.DeleteAll();
        Level = 1;
        LevelCompletedCount = 1;
        PlayerPrefs.SetInt("IsFirstStart", 1);
        PlayerPrefs.SetInt("tutorialJoystick", 0);
        PlayerPrefs.SetInt("tutorialButtons", 0);
        PlayerPrefs.SetInt("LevelCompletedCount", 1);
        Save();
    }

    private void Load()
    {
        if (PlayerPrefs.HasKey("IsFirstStart"))
        {
            Level = PlayerPrefs.GetInt("Level");
            LevelCompletedCount = PlayerPrefs.GetInt("LevelCompletedCount", Level);
        }else 
            LoadDefault();

        //LoadLevel();
    }

    public void Save()
    {
        PlayerPrefs.SetInt("Level", Level);
        PlayerPrefs.SetInt("LevelCompletedCount", LevelCompletedCount);
        PlayerPrefs.Save();
    }

    public void LoadLevel()
    {
        if (Level == 0)
            Level = 1;
        SceneManager.LoadScene(Level);
    }
    [ContextMenu("TestNextLevel")]
    public void NextLevel()
    {
        Level++;
        LevelCompletedCount++;
        if (Level > LevelMax)
            Level = 10;
        Save();
        LoadLevel();
    }

}
