﻿using System.Collections;
using UnityEngine;

public class FinishZone : MonoBehaviour
{
    public GameObject hawkGameObject;
    public AnimationCurve flyingCurve;
    public Collider target;
    public Transform leftMoveTarget;
    public float timerFlying = 1f;
    public float timerMoveLeft = 0.5f;
    public float jumpHeight = 1f;


    private PlayerController player;
    private void OnTriggerEnter(Collider other)
    {
        if(other.isTrigger) return;
        player = other.GetComponent<PlayerController>();
        if(player == null) return;

        if (player.IsJumping) return;
        ObserverPattern.Instance.OnCloseUIElementsAction?.Invoke();
        player.playerRigidbody.isKinematic = true;
        StartCoroutine(JumpObject(other));
    }

    private IEnumerator JumpObject(Collider obj)
    {
        float time = 0f;
        Vector3 startPoint = obj.transform.position;
        Vector3 endPoint = target.bounds.center + Vector3.up * obj.bounds.size.y;

        if (player != null) player.IsJumping = true;
        Quaternion startRotate = obj.transform.rotation;
        while (time < timerFlying)
        {
            time += Time.deltaTime;
            float animateCurve = flyingCurve.Evaluate(time / timerFlying);
            obj.transform.position = new Vector3(Mathf.Lerp(startPoint.x, endPoint.x, animateCurve),
                startPoint.y + animateCurve * (endPoint.y - obj.bounds.min.y + obj.bounds.size.y * jumpHeight), Mathf.Lerp(obj.transform.position.z, endPoint.z, animateCurve));
            yield return null;
        }

        time = 0;
        startPoint = obj.transform.position;
        endPoint = new Vector3(leftMoveTarget.position.x, obj.transform.position.y, leftMoveTarget.transform.position.z);
        startRotate = obj.transform.rotation;
        while (time < timerMoveLeft)
        {
            time += Time.deltaTime;
            obj.transform.position = Vector3.Lerp(startPoint, endPoint, time / timerMoveLeft);
            obj.transform.rotation = Quaternion.Lerp(startRotate, Quaternion.LookRotation(endPoint - startPoint, Vector3.up), time/timerMoveLeft*3);
            yield return null;
        }

        obj.gameObject.SetActive(false);
        if (player != null)
            CameraFollow.instance.target = null;
        ObserverPattern.Instance.OnCheckWinAction?.Invoke();
    }
}
