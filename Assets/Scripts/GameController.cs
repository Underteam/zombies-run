﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    public static GameController Instance = null;

    private void Awake()
    {
        Instance = this;
        GetSkinsPeople = Resources.Load<SkinsPeopleConfig>("SkinsPeopleConfig");
        GetSkinsZombie = Resources.Load<SkinsZombieConfig>("SkinsZombieConfig");
        GetCarContainer = Resources.Load<CarContainer>("CarContainer");

        if (PlayerData.Instance == null)
        {
            var obj = Instantiate(new GameObject("PlayerData"));
            obj.AddComponent<PlayerData>();
        }
    }

    public bool IsGameStarted { get; set; }
    public bool IsCanWin { get; set; }
    public bool IsDontCanControl { get; set; }
    public bool IsNeedSavingPeople { get; set; }

    public PlayerController Player { get; set; }
    public SkinsPeopleConfig GetSkinsPeople { get; set; }
    public SkinsZombieConfig GetSkinsZombie { get; set; }
    public CarContainer GetCarContainer { get; set; }
    public List<ZombieController> GetZomvies { get; set; } = new List<ZombieController>();

    [Header("Выбор управления")]
    public VariableJoystick joystick;
    //[Space]
    public bool IsMoveLeft { get; set; }
    public bool IsMoveRight { get; set; }
    public bool IsAutoRun { get; set; }
    public FailTypeEnum FailType { get; set; }

    public UnityEvent onStart;
    public UnityEvent onGameStart;

    private void Start()
    {
        onStart?.Invoke();
        ObserverPattern.Instance.OnStartLevelAction += delegate
        {
            onGameStart.Invoke();
            ControlActivate(LoadControl());
        };
        ShowTutorial(LoadControl());
    }

    public void ShowTutorial(string type)
    {
        if (PlayerPrefs.GetInt(type + "tutorialShow", 0) == 0 || PlayerPrefs.GetInt(type + "tutorialShow", 0) == 1)
        {
            controlTypeActivators.Find(c => c.controlType == type).onTutorialShow?.Invoke();
            PlayerPrefs.SetInt(type + "tutorialShow", 1);
        }
    }

    public void DisableControl()
    {
        controlTypeActivators.ForEach((c) => c.onDisactivate.Invoke());
    }

    public void EnableControl(string controlType)
    {

    }

    [HideInInspector]
    public ControlTypeEnum ControlType = ControlTypeEnum.JoystickControlAll;

    private void ControlActivate(string controlType)
    {
        //IsGameStarted = true;
        int index = controlTypeActivators.FindIndex((c) => c.controlType == controlType);
        if (index < 0)
            return;

        ControlType = controlTypeActivators[index].type;
        controlTypeActivators[index].onActivate.Invoke();
    }

    public List<ControlTypeActivator> controlTypeActivators = new List<ControlTypeActivator>();
    [System.Serializable]
    public class ControlTypeActivator
    {
        public string controlType = "";
        public ControlTypeEnum type;
        public UnityEvent onActivate;
        public UnityEvent onDisactivate;
        public UnityEvent onTutorialShow;
        public UnityEvent onTutorialHide;
    }

    public void TestCount()
    {
        Debug.LogError("control count =  " + controlTypeActivators.Count);
    }

    public void StopTutorial()
    {
        for (int i = 0; i < controlTypeActivators.Count; i++)
        {
            controlTypeActivators[i].onTutorialHide?.Invoke();
        }
    }

    public string defaultControlType = "JoystickControlAll";
    private string LoadControl()
    {
        return PlayerPrefs.GetString("Control", defaultControlType);
    }

    public void ChooseControlType(string controlType)
    {
        PlayerPrefs.SetString("Control", controlType);

        for (int i = 0; i < controlTypeActivators.Count; i++)
        {
            if (controlTypeActivators[i].onTutorialHide != null)
                controlTypeActivators[i].onTutorialHide.Invoke();
        }
        ShowTutorial(controlType);
    }

    public void ButtonLeftAction(bool isActive)
    {
        IsMoveLeft = isActive;
    }

    public void ButtonRightAction(bool isActive)
    {
        IsMoveRight = isActive;
    }
}
public enum ControlTypeEnum
{
    JoystickControlAll,
    ButtonsControl
}
