﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ItemProgressBar : MonoBehaviour
{
    [Header("Прогесс активности предмета")]
    public GameObject progressBackBar;
    public Image progressBar;
    [Range(3f, 5f)]
    public float itemMaxTime;
    private float currentItemTime;

    private void Start()
    {
        ObserverPattern.Instance.OnItemActivateAction += ItemActive;
        ObserverPattern.Instance.OnItemDeactivateAction += ItemDeactive;
        ObserverPattern.Instance.OnItemShowAction += ItemShow;
        ObserverPattern.Instance.OnItemHideAction += ItemHide;
        if(progressBackBar != null)
            progressBackBar.SetActive(false);
        else if(progressBar != null)
        {
            progressBar.gameObject.SetActive(false);
        }

    }

    private void ItemActive()
    {
        ObserverPattern.Instance.OnPlayerCanTurnAction?.Invoke(true);
        progressBar.fillAmount = 0f;
        if(progressBackBar != null)
            progressBackBar.SetActive(true);
        else progressBar.gameObject.SetActive(true);
        if (itemTimerCoroutine != null)
            currentItemTime = 0f;
        else
            itemTimerCoroutine = StartCoroutine(StartItemTimer());
    }

    private void ItemDeactive()
    {
        ObserverPattern.Instance.OnPlayerCanTurnAction?.Invoke(false);
        ObserverPattern.Instance.OnRemoveItemAction?.Invoke();

        if (itemTimerCoroutine != null)
            StopCoroutine(itemTimerCoroutine);

        itemTimerCoroutine = null;
        if(progressBackBar != null)
            progressBackBar.SetActive(false);
        else progressBar.gameObject.SetActive(false);
    }

    private void ItemHide()
    {
        ObserverPattern.Instance.OnPlayerCanTurnAction?.Invoke(false);
        if (itemTimerCoroutine != null)
            StopCoroutine(itemTimerCoroutine);

        itemTimerCoroutine = null;
        if (progressBackBar != null)
            progressBackBar.SetActive(false);
        else progressBar.gameObject.SetActive(false);
    }

    private void ItemShow()
    {
        ObserverPattern.Instance.OnPlayerCanTurnAction?.Invoke(true);
        progressBar.fillAmount = 0f;
        if (progressBackBar != null)
            progressBackBar.SetActive(true);
        else progressBar.gameObject.SetActive(true);
        if (itemTimerCoroutine != null)
            currentItemTime = 0f;
        else
            itemTimerCoroutine = StartCoroutine(StartItemTimer());
    }

    private Coroutine itemTimerCoroutine;
    private IEnumerator StartItemTimer()
    {
        currentItemTime = 0;
        while (currentItemTime < itemMaxTime)
        {
            currentItemTime += Time.deltaTime;
            progressBar.fillAmount = (itemMaxTime - currentItemTime) / itemMaxTime;
            yield return null;
        }
        itemTimerCoroutine = null;
        if(progressBackBar != null)
            progressBackBar.SetActive(false);
        else
            progressBar.gameObject.SetActive(false);
        ObserverPattern.Instance.OnPlayerCanTurnAction?.Invoke(false);
        ObserverPattern.Instance.OnRemoveItemAction?.Invoke();
    }
}
