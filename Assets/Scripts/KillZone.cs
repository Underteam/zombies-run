﻿using UnityEngine;

public class KillZone : MonoBehaviour
{
    public ZombieController zombie;
    public ParticleSystem effect;

    private PlayerController player;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null)
        {
            player = other.GetComponent<PlayerController>();
            if(player == null) return;
            if (player.IsCanTurn)
            {
                zombie.TurnIntoPeople();
                ObserverPattern.Instance.OnItemDeactivateAction?.Invoke();
                effect.Play();
                gameObject.SetActive(false);
            }
            else if(!zombie.IsPeople)
            {
                GameController.Instance.FailType = FailTypeEnum.DeathFromZombie;
                ObserverPattern.Instance.OnPlayerDeathAction?.Invoke();
                Invoke("ZombieStateIdle", 0.5f);
            }
        }
    }

    private void ZombieStateIdle()
    {
        ObserverPattern.Instance.OnZombieStateIdleAction?.Invoke();
    }
}
