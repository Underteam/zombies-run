﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ObjectRepeater : MonoBehaviour
{
    public MeshRenderer plane;

    [Header("Расстояние после которого обьект будет повторяться")]
    public float distanceToRepeat = 0;

    [Header("Отступ")]
    public Vector3 offset = Vector3.zero;

    BoxCollider trigger;

    [Header("Следующий объект")]
    public ObjectRepeater prevObj = null;

    [Header("Точки движения для машин")]
    public List<Transform> StartPoints = new List<Transform>();

    private void Start()
    {
        if (!Application.isPlaying || trigger != null)
            return;

        AddCollider();
    }

    public void AddCollider()
    {
        trigger = gameObject.AddComponent<BoxCollider>();

        trigger.size = Vector3.one * distanceToRepeat;
        trigger.center = offset;
        trigger.isTrigger = true;
    }


    public void SetNextPos()
    {
        transform.position = prevObj.transform.position + Vector3.right * (prevObj.distanceToRepeat + prevObj.offset.x);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position + offset, Vector3.one * distanceToRepeat);
    }

   // [Header("Учавствующие слои")]
   // public List<int> usingLayers = new List<int>();

    //private void OnTriggerExit(Collider other)
    //{
    //    if (!GameController.Instance.IsGameStarted)
    //        return;

    //    if (usingLayers.Contains(other.gameObject.layer))
    //    {
    //        NextPos();
    //    }
    //}

    //public void NextPos()
    //{
    //    if (prevObj.transform.position.x < transform.position.x)
    //        prevObj.NextPos();
    //    StartCoroutine(SetNextPos(RepeaterManager.instance.setNextPosDelay, SetNextPos));
    //}

    //public void StopAll()
    //{
    //    StopAllCoroutines();
    //}

    //public IEnumerator SetNextPos(float delay, System.Action act)
    //{
    //    yield return new WaitForSeconds(delay);
    //    act?.Invoke();
    //}
}