﻿using System.Collections.Generic;
using UnityEngine;

public class RepeaterManager : MonoBehaviour {

    public int repeatersCount = 4;

    [HideInInspector]
    public List<ObjectRepeater> repeaters = new List<ObjectRepeater> ();
    public List<ObjectRepeater> DefaultRepeatersPrefabsList = new List<ObjectRepeater>();
    private List<ObjectRepeater> repeatersList = new List<ObjectRepeater>();

    int currentRepeaterIndex = 0;

    [HideInInspector]
    public int endRepeater;

    [Header ("Через сколько появится следующее поле")]
    public float setNextPosDelay = 5;

    public void SetCurrentRepeaterIndexToMax ()
    {
        currentRepeaterIndex = repeaters.Count - 1;
    }

    public static RepeaterManager instance = null;

    private void Awake () {
        instance = this;
    }

    public List<Vector3> startPoses = new List<Vector3> ();

    private void Start ()
    {
        GenerateCity();
    }

    

    public void GenerateCity()
    {
        if(DefaultRepeatersPrefabsList.Count <= 0) return;

        repeatersList = DefaultRepeatersPrefabsList;

        var startObj =  Instantiate(DefaultRepeatersPrefabsList[UnityEngine.Random.Range(0, DefaultRepeatersPrefabsList.Count)]);
        repeaters.Add(startObj);
        startPoses.Add(startObj.transform.position);
        for (int i = 1; i < repeatersCount; i++)
        {
            if (repeatersList.Count <= 0) repeatersList = DefaultRepeatersPrefabsList;

            int index = i == 1 ? 1 : UnityEngine.Random.Range(0, repeatersList.Count);

            ObjectRepeater obj = Instantiate(repeatersList[index]);
            obj.prevObj = repeaters[i - 1];
            obj.transform.position = repeaters[i - 1].transform.position + Vector3.right * (repeaters[i - 1].distanceToRepeat + repeaters[i - 1].offset.x);
            repeaters.Add(obj);
            startPoses.Add(obj.transform.position);

            var temp = new List<ObjectRepeater>();

            for (int j = 0; j < repeatersList.Count; j++)
            {
                if(index != j) temp.Add(repeatersList[j]);
            }
            repeatersList = temp;
        }

        startObj.prevObj = repeaters[repeaters.Count - 1];
    }

    public void Restart()
    {
        for (int i = 0; i < repeaters.Count; i++)
        {
            repeaters[i].transform.position = startPoses[i];
           // repeaters[i].StopAll();
        }
    }
}

    #region Random
    public class RandomNotRepeat : System.Random
    {
        int _min, _max;
        private float minF, maxF;

        Stack<int> list;
        private Stack<float> listF;
        /// <summary>
        /// Инициализирует новый экземпляр класса System.Random с помощью зависимого
        //  от времени начального значения по умолчанию.
        /// </summary>
        /// <param name="min">Включенной нижний предел возвращаемого случайного числа.</param>
        /// <param name="max">Исключенный верхний предел возвращаемого случайного числа. Значение maxValue должно быть больше или равно значению minValue.</param>
        public RandomNotRepeat(int minValue, int maxValue)
        {
            _max = maxValue;
            _min = minValue;
            genList();
        }

        public RandomNotRepeat(float minValue, float maxValue)
        {
            maxF = maxValue;
            minF = minValue;
            genFloatList();
        }
    /// <summary>
    /// Генерация списка исключений
    /// </summary>
    protected void genList()
        {
            System.Random rand = new System.Random();
            List<int> temp = new List<int>();
            for (int i = _min; i < _max; i++)
            {
                temp.Add(i);
            }
            list = new Stack<int>();
            while (temp.Count > 0)
            {
                int addInt = temp[rand.Next(0, temp.Count)];
                list.Push(addInt);
                temp.Remove(addInt);
            }
        }

        protected void genFloatList()
        {
            System.Random rand = new System.Random();
            List<float> temp = new List<float>();

            for (float i = minF; i < maxF; i++)
            {
                temp.Add(i);
            }

            listF = new Stack<float>();

            while (temp.Count > 0)
            {
                float addInt = temp[rand.Next(0, temp.Count)];
                listF.Push(addInt);
                temp.Remove(addInt);
            }
        }
    /// <summary>
    /// Возвращает неотрицательное случайное число.
    /// </summary>
    /// <returns>32-разрядное целое число со знаком большее или равное minValue и меньше, чем maxValue; то есть, диапазон возвращаемого значения включает minValue, не включает maxValue. Если значение параметра minValue равно значению параметра maxValue, то возвращается значение minValue.</returns>
    public override int Next()
        {
            if (list.Count > 0)
            {
                return list.Pop();
            }
            else
            {
                genList();
            }
            return list.Pop();
        }

    public override double NextDouble()
    {
        if (listF.Count > 0)
        {
            return listF.Pop();
        }
        else
        {
            genFloatList();
        }
        return listF.Pop();
    }
}

    #endregion