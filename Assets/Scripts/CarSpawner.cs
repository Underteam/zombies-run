﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
    public Transform rightSpawnPoint;
    public Transform leftSpawnPoint;
    public List<Car> carPrefabList = new List<Car>();
    public float minDelay = 0.5f;
    public float maxDelay = 1f;
    public int poolCount = 4;
    private float speedCar = 0.1f;

    private readonly List<Car> carRespawnList = new List<Car>();
    private readonly List<Car> carPoolList = new List<Car>();

    private void OnTriggerExit(Collider other)
    {
        Car tempCar = other.GetComponent<Car>();

        if(tempCar != null)
            CarIsOut(tempCar);
    }

    private void Start()
    {
        if (GameController.Instance.GetCarContainer != null && GameController.Instance.GetCarContainer.carList.Count > 0)
            carPrefabList = GameController.Instance.GetCarContainer.carList;
        ObserverPattern.Instance.OnStopAllCarAction += StopCars;
        ObserverPattern.Instance.OnClearPoolCarsAction += ClearPoolCars;
        Init();
    }

    private void Init()
    {
        if(carPrefabList.Count <= 0) return;
        
        for (int i = 0; i < poolCount; i++)
        {
           CarSpawn();
        }

        carSpawnCoroutine = StartCoroutine(cr_Spawn());
    }

    private void CarSpawn()
    {
        int carRandom = Random.Range(0, carPrefabList.Count);
        Transform point = null;
        int pointIndex = Random.Range(0, 1);
        point = pointIndex == 0 ? leftSpawnPoint : rightSpawnPoint;

        Car car = Instantiate(carPrefabList[carRandom]);

        car.transform.position = point.position;
        car.transform.rotation = point.rotation;
        car.Init(speedCar);
        car.gameObject.SetActive(false);
        carRespawnList.Add(car);
        carPoolList.Add(car);
    }

    private void StopCars()
    {
        for (int i = 0; i < carPoolList.Count; i++)
        {
            carPoolList[i].IsMove = false;
        }
    }

    private void ClearPoolCars()
    {
        for (int i = 0; i < carPoolList.Count; i++)
        {
            CarIsOut(carPoolList[i]);
        }
    }

    private Coroutine carSpawnCoroutine;

    private IEnumerator cr_Spawn()
    {
        int prevIndex = -1;
        Transform point = null;
        while (carRespawnList.Count > 0)
        {
            if (carRespawnList.Count > 0)
            {
                for (int i = 0; i < carRespawnList.Count; i++)
                {
                    int pointIndex = Random.Range(0, 1);
                    if (prevIndex == pointIndex)
                        pointIndex = pointIndex == 0 ? 1 : 0;

                    prevIndex = pointIndex;
                    point = pointIndex == 0 ? leftSpawnPoint : rightSpawnPoint;
                    yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));

                    int carIndex = Random.Range(0, carRespawnList.Count);
                    Car car = carRespawnList[carIndex];

                    car.transform.position = point.position;
                    car.transform.rotation = point.rotation;
                    car.gameObject.SetActive(true);
                    carRespawnList.Remove(car);
                }
            }
        }

        carSpawnCoroutine = null;
    }


    public void CarIsOut(Car car)
    {
        carRespawnList.Add(car);
        car.gameObject.SetActive(false);
        if (carSpawnCoroutine == null)
            carSpawnCoroutine = StartCoroutine(cr_Spawn());
    }

}
