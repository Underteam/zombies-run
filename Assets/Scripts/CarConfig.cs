﻿using UnityEngine;

[CreateAssetMenu(fileName = "CarConfig", menuName = "Data/CarConfig")]
public class CarConfig : ScriptableObject
{
    public Mesh CarMesh;
}
