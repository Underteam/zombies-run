﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SkinsZombieConfig", menuName = "Data/SkinsZombieConfig")]
public class SkinsZombieConfig : ScriptableObject
{
    public SkinConfig defaultZombieSkin;
    public List<SkinConfig> zombieSkinList = new List<SkinConfig>();

    public SkinConfig GetRandomSkin()
    {
        if (zombieSkinList.Count <= 0)
        {
            Debug.LogError("Skins zombie config = null");
            return defaultZombieSkin != null ? defaultZombieSkin : null;
        }
        int randomIndex = Random.Range(0, zombieSkinList.Count);
        return zombieSkinList[randomIndex];
    }
}
