﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnZombiesIntoPeople : MonoBehaviour
{
    [Header("Кол-во зомби необходимое превратить в людей")]
    public int TurnZombieIntoPeopleCounts;
    [Header("Кол-во превращенных человек")]
    public int CurrentNumbersZombieIntoPeople;

    public GameObject ChangeNumbersTurnAnimation;
    [Header("Прогресс спасенных людей")]
    public Animator peopleTextAnimator;
    public Text peopleSavingText;
    public Text playerPeopleSaving;

    private bool isNeedAll = false;

    private void Start()
    {
        ObserverPattern.Instance.OnChangeNumbersZobieIntoPeopleAction += SetNumbersZobieIntoPeople;
        ObserverPattern.Instance.OnAddPeopleAction += AddPeople;
        //ObserverPattern.Instance.OnRemovePeopleAction += RemovePeople;
        ObserverPattern.Instance.OnCheckWinAction += CheckWin;
        FindItems();

        IsAllTurnZombiesIntoPeople isAll = FindObjectOfType<IsAllTurnZombiesIntoPeople>();
        isNeedAll = isAll != null && isAll.isAllTurn;

        if (!isNeedAll) TurnZombieIntoPeopleCounts -= 1;

        if (CurrentNumbersZombieIntoPeople <= TurnZombieIntoPeopleCounts)
        {
            peopleSavingText.text = CurrentNumbersZombieIntoPeople + "/" + TurnZombieIntoPeopleCounts;
            playerPeopleSaving.text = CurrentNumbersZombieIntoPeople + "/" + TurnZombieIntoPeopleCounts;
        }

        if (TurnZombieIntoPeopleCounts == 0)
        {
            GameController.Instance.IsNeedSavingPeople = false;
            GameController.Instance.IsCanWin = true;
        }
        else
        {
            GameController.Instance.IsNeedSavingPeople = true;
        }
        
        Invoke("SetTextPosition", 0.02f);
        Invoke("SetTextPosition", 0.04f);
        Invoke("SetTextPosition", 0.08f);
        Invoke("SetTextPosition", 0.15f);
    }

    private Transform target;


    [ContextMenu("FindItems")]
    private void FindItems()
    {
        int count = FindObjectsOfType<Item>().Length;
        TurnZombieIntoPeopleCounts = count;
    }

    private void SetTextPosition()
    {
        target = GameController.Instance.Player.peopleSavingTextPosition;
        Debug.Log("SetPosition");
        if (playerPeopleSaving != null)
        {
            playerPeopleSaving.transform.position = Camera.main.WorldToScreenPoint(target.position);
        }
    }

    private void SetNumbersZobieIntoPeople(int value)
    {
        CurrentNumbersZombieIntoPeople += value;

        if (CurrentNumbersZombieIntoPeople <= TurnZombieIntoPeopleCounts)
        {
            if(peopleTextAnimator != null) peopleTextAnimator.Play("ChangeFontSize");
            peopleSavingText.text = CurrentNumbersZombieIntoPeople + "/" + TurnZombieIntoPeopleCounts;
            playerPeopleSaving.text = CurrentNumbersZombieIntoPeople + "/" + TurnZombieIntoPeopleCounts;
        }


        if (CurrentNumbersZombieIntoPeople == TurnZombieIntoPeopleCounts)
        {
            GameController.Instance.IsCanWin = true;
            CameraFollow.instance.confettiEffect.SetActive(true);
        }

        ChangeNumbersTurnAnimation.SetActive(true);
    }

    public readonly List<ZombieController> peopleSavingList = new List<ZombieController>();

    private void AddPeople(ZombieController people)
    {
        peopleSavingList.Add(people);
    }

    private void CheckWin()
    {
        if (GameController.Instance.IsCanWin)
        {
            ObserverPattern.Instance.OnHawkMoveUpAction?.Invoke();
            Invoke("WinAction", 0.5f);
        }
        else
        {
            GameController.Instance.FailType = FailTypeEnum.SavedNotAll;
            ObserverPattern.Instance.OnOpenFailPanelAction?.Invoke();
        }
    }

    private void WinAction()
    {
        ObserverPattern.Instance.OnPlayerWinAction?.Invoke();
    }
}
