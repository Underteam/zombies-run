﻿using UnityEngine;
using UnityEngine.AI;

[ExecuteInEditMode]
public class SkinedController : MonoBehaviour
{
    public NavMeshAgent agent;
    public SkinnedMeshRenderer currentSkinedMesh;
    [Header("Выбираем и ставим скин зомби")]
    public SkinConfig zombieSkinConfig;

    [Header("Выбираем и ставим скин человека, в которого превращается зомби")]
    public SkinConfig peopleSkinConfig;


    private void Start()
    {
        if(GameController.Instance != null && zombieSkinConfig == null)
            zombieSkinConfig = GameController.Instance.GetSkinsZombie.GetRandomSkin();
        SetZombieSkin();
        SetPeopleSkinConfig();
    }

    private void SetZombieSkin()
    {
        if (gameObject.GetComponent<NavMeshAgent>() != null && agent == null)
            agent = gameObject.GetComponent<NavMeshAgent>();

        if (zombieSkinConfig != null)
        {
            //agent.speed = zombieSkinConfig.speedMove;
            currentSkinedMesh.sharedMesh = zombieSkinConfig.skinMesh;
            currentSkinedMesh.material = zombieSkinConfig.skinMaterial;
        }
    }

    private void SetPeopleSkinConfig()
    {
        if (peopleSkinConfig == null && GameController.Instance != null)
            peopleSkinConfig = GameController.Instance.GetSkinsPeople.GetRandomSkin();
    }

    public void SetPeopleSkin()
    {
        currentSkinedMesh.sharedMesh = peopleSkinConfig.skinMesh;
        currentSkinedMesh.material = peopleSkinConfig.skinMaterial;
    }


#if UNITY_EDITOR
    private void OnValidate()
    {
        SetZombieSkin();
        SetPeopleSkinConfig();
    }
#endif



}
