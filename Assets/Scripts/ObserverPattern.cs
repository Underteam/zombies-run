﻿using System;
using UnityEngine;

public class ObserverPattern : MonoBehaviour
{
    public static ObserverPattern Instance;
    public Action OnPlayerDeathAction;
    public Action OnZombieStateIdleAction;
    public Action<int> OnChangeNumbersZobieIntoPeopleAction;
    public Action<bool> OnPlayerCanTurnAction;
    public Action OnItemActivateAction;
    public Action OnItemDeactivateAction;
    public Action OnRemoveItemAction;
    public Action OnPlayerWinAction;
    public Action OnPlayerFailAction;
    public Action OnOpenFailPanelAction;
    public Action OnStopAllCarAction;
    public Action OnPlayerStateIdleAction;
    public Action<ZombieController> OnAddPeopleAction;
    public Action<ZombieController> OnRemovePeopleAction;
    public Action OnHawkMoveUpAction;
    public Action<Vector3> OnDeathForceAction;
    public Action<float, float> OnChangeProgressLevelAction;
    public Action OnControlActivateAction;
    public Action OnCloseUIElementsAction;
    public Action OnStartLevelAction;
    public Action OnCheckWinAction;
    public Action OnOpenTapToStartAction;
    public Action OnRevivePlayerAction;
    public Action OnClearPoolCarsAction;
    public Action OnShowUIAction;
    public Action OnRestartAgentAction;
    public Action OnItemShowAction;
    public Action OnItemHideAction;

    private void Awake()
    {
        Instance = this;
    }

}
