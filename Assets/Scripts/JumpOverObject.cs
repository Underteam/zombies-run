﻿using UnityEngine;

public class JumpOverObject : MonoBehaviour
{
    public BoxCollider collider;
    public AnimationCurve flyinAmplitudeCurve;
    public AnimationCurve moveForwardCurve;
    public float jumpTime = 1f;
    private PlayerController player;
    public float jumpDistance = 1f;
    public float jumpHeight = 1f;

    private JumpObject objectJump;

    private void OnTriggerEnter(Collider other)
    {
        if(other.isTrigger) return;

        objectJump = other.GetComponent<JumpObject>();

        if(objectJump == null) return;
        if(objectJump.transform.position.x > transform.position.x) return;

        objectJump.Jump(collider, jumpDistance, jumpTime, jumpHeight, flyinAmplitudeCurve, moveForwardCurve);
    }
}
