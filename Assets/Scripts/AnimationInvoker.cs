﻿using UnityEngine;
using UnityEngine.Events;

public class AnimationInvoker : MonoBehaviour
{
    public UnityEvent Event;

    public void AnimationInvoke()
    {
        Event?.Invoke();
    }

    public void ObjectDestroy(GameObject obj)
    {
        Destroy(obj.gameObject);
    }

    public void JumpingDeactive()
    {
        Event?.Invoke();
    }

}
