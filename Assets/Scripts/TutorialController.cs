﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    public static TutorialController Instance = null;
    public GameObject tutorialJoystick;
    public GameObject tutorialButtons;

    public Button closeTutorial;

    private void Awake()
    {
        Instance = this;
    }

    public void Start()
    {
        if(closeTutorial != null)
            closeTutorial.onClick.AddListener(CloseTutorial);

        bool isTutorialStart = PlayerPrefs.HasKey("isTutorialStart");

        if (isTutorialStart)
        {
            CloseTutorial();
        }
    }


    public void OpenJoystick()
    {
        tutorialJoystick.SetActive(true);
        closeTutorial.gameObject.SetActive(true);
        PlayerPrefs.SetInt("tutorialJoystick", 1);
        PlayerPrefs.Save();
    }

    public void OpenButtons()
    {
        tutorialButtons.SetActive(true);
        closeTutorial.gameObject.SetActive(true);
        PlayerPrefs.SetInt("tutorialButtons", 1);
        PlayerPrefs.Save();
    }


    private void CloseTutorial()
    {
        ObserverPattern.Instance.OnOpenTapToStartAction?.Invoke();
        GameController.Instance.IsGameStarted = false;
        tutorialButtons.SetActive(false);
        tutorialJoystick.SetActive(false);
        gameObject.SetActive(false);
        
    }
}
