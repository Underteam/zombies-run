﻿using System.Collections;
using UnityEngine;

public class JumpObject : MonoBehaviour
{
    public Collider objectCollider;
    public Rigidbody objectRigidbody;

    private float jumpDistance;
    private float jumpTime;
    private float jumpHeight = 1f;
    private AnimationCurve flyinAmplitudeCurve;
    private AnimationCurve moveForwardCurve;
    private Collider targetCollider;

    private PlayerController player;
    private ZombieController zombie;

    private void Start()
    {
        player = objectCollider.GetComponent<PlayerController>();
        zombie = objectCollider.GetComponent<ZombieController>();
    }

    private bool isJumpAnimate = false;
    public void Jump(Collider obj, float jumpDistance, float jumpTime, float jumpHeight, AnimationCurve flyingCurve, AnimationCurve moveForwardCurve)
    {
        targetCollider = obj;
        this.jumpDistance = jumpDistance;
        this.jumpTime = jumpTime;
        this.jumpHeight = jumpHeight;
        this.flyinAmplitudeCurve = flyingCurve;
        this.moveForwardCurve = moveForwardCurve;

        if (player != null && player.IsJumping) return;
        if (zombie != null && zombie.IsJumping) return;
        if(zombie != null && !zombie.IsPeople) return;

        isJumpAnimate = false;

        if (player != null && !player.isCarryItem)
        {
            isJumpAnimate = true;
            player.playerAnimator.Play("Jump");
        }


        if (zombie != null)
            zombie.zombieAnimator.Play("Jump");
        objectRigidbody.isKinematic = true;
        StartCoroutine(JumpPingOverIt());
    }

    private Vector3? startPoint;
    private Vector3? endPoint;

    private IEnumerator JumpPingOverIt()
    {
        float time = 0f;
        startPoint = objectCollider.transform.position;
        endPoint = new Vector3(targetCollider.bounds.max.x + objectCollider.bounds.size.x * jumpDistance, startPoint.Value.y, startPoint.Value.z);
        
        if(player != null) player.IsJumping = true;
        if (zombie != null) zombie.IsJumping = true;
        while (time < jumpTime)
        {
            time += Time.deltaTime;
            float animateCurva = flyinAmplitudeCurve.Evaluate(time / jumpTime);
            float moveCurva = moveForwardCurve.Evaluate(time / jumpTime);
            objectCollider.transform.position = new Vector3(Mathf.Lerp(startPoint.Value.x, endPoint.Value.x, moveCurva), startPoint.Value.y + animateCurva * (targetCollider.bounds.max.y - objectCollider.bounds.min.y + objectCollider.bounds.size.y * jumpHeight), objectCollider.transform.position.z);
            yield return null;
        }
        if(!isJumpAnimate && player != null) player.JumpingActive(false);
        objectRigidbody.isKinematic = false;
    }
}
